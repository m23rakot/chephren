export {};

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $filters: {
      longDate(value: Date | dayjs.Dayjs): string;
    };
  }
}
