import { Command, type NewCommand } from '@/models/comand.model';
import type { Node } from '@/models/node.model';
import type { Occurence } from '@/models/occurence.type';
import { Resource, type NewResource } from '@/models/resource.model';
import { ToastService } from '@/services/toast.service';
import { useNodesStore } from '@/stores/nodes.store';
import { defineStore, storeToRefs } from 'pinia';
import { computed, ref } from 'vue';

export const useResourcesStore = defineStore('resources', () => {
  const resourcesUrlSuffix = '/api/resources';
  const resourceUrlSuffix = '/api/resource';
  const nodesStore = useNodesStore();

  const updateUrl = (resourceId: string) => `/api/resource/${resourceId}`;

  /**
   * Map of resources by node id
   */
  const resourcesMap = ref(new Map<string, Resource[]>());
  const occurencesMap = ref(new Map<string, Occurence[]>());

  async function fetchResourceExist(node: Node, resourceId: string): Promise<Occurence> {
    const response = await fetch(`${node.address}${resourceUrlSuffix}/${resourceId}`, {
      method: 'GET',
    });

    if (!response.ok) {
      throw new Error(await response.json());
    }

    const json = await response.json();
    return { resource: new Resource(json), node: node, checked: false };
  }

  async function fetchResourceOccurences(
    resourceId: string,
    force: boolean = false,
  ): Promise<void> {
    if (force) {
      occurencesMap.value.delete(resourceId);
    }

    if (occurencesMap.value.has(resourceId)) return;

    const { nodes } = storeToRefs(nodesStore);

    if (!nodes.value) {
      await nodesStore.fetchNodes();
      if (!nodes.value) {
        return;
      }
    }

    const occ: Occurence[] = [];

    const promises = [];
    for (const node of nodes.value) {
      const promise = fetchResourceExist(node, resourceId);
      promises.push(promise);
    }

    const results = await Promise.allSettled(promises);

    for (const result of results) {
      if (result.status === 'fulfilled') {
        const occurence = result.value;
        occ.push(occurence);
      }
    }

    occurencesMap.value.set(resourceId, occ);
  }

  async function fetchResourcesForNode(node: Node, force: boolean = false): Promise<void> {
    if (force) {
      clearResources();
    }

    if (resourcesMap.value.has(node.id)) return;

    try {
      const response = await fetch(`${node.address}${resourcesUrlSuffix}`, {
        method: 'GET',
      });

      if (!response.ok) {
        ToastService.addError(`Failed to fetch resources from ${node.name}`, response.statusText);
        console.error(`Failed to fetch resources from ${node.name}`, response);
        return;
      }

      const json = await response.json();
      resourcesMap.value.set(
        node.id,
        json.map((resource: any) => new Resource(resource)),
      );
    } catch (e: any) {
      ToastService.addError(`Failed to fetch resources from ${node.name}`, e.message);
      console.error(`Failed to fetch resources from ${node.name}`, e);
      return;
    }
  }

  async function deleteResource(
    node: Node,
    resourceId: string,
    otherNodes: URL[] = [],
    fetchNode: boolean = false,
  ) {
    if (!resourcesMap.value.has(node.id)) return;

    try {
      const response = await fetch(`${node.address}${resourceUrlSuffix}/${resourceId}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          addresses: otherNodes,
        }),
      });

      if (!response.ok) {
        const error = await response.json();
        throw new Error(error.message);
      }

      const resource = resourcesMap.value.get(node.id)?.find((r) => r.id === resourceId);
      if (resource) {
        resource.deleted = true;
        resource.commands?.push(
          new Command({
            command: 'DELETE',
            date: new Date(),
            id: '0',
          }),
        );
      }

      if (fetchNode) {
        await fetchResourcesForNode(node, true);
      }
    } catch (error: any) {
      console.error(error);
      ToastService.addError('Error deleting resource', error.message);
    }
  }

  async function updateResource(node: Node, resource: Resource, newCommand: NewCommand) {
    const url = updateUrl(resource.id);

    try {
      const response = await fetch(`${node.address}${url}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          command: newCommand.command,
          addresses: newCommand.addresses ?? [],
        }),
      });

      if (!response.ok) {
        const error = await response.json();
        throw new Error(error.message);
      }

      resource.commandsCount ??= 1;
      resource.commandsCount += 1;
    } catch (error: any) {
      console.error(error);
      ToastService.addError('Error updating resource', error.message);
      throw error;
    }
  }

  async function createResource(node: Node, newResource: NewResource) {
    if (!newResource.name || !newResource.command) {
      throw new Error('Name and command are required');
    }
    newResource.addresses ??= [];

    try {
      const response = await fetch(`${node.address}${resourcesUrlSuffix}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newResource),
      });

      if (!response.ok) {
        const error = await response.json();
        throw new Error(error.message);
      }

      node.resourcesCount ??= 0;
      node.resourcesCount += 1;

      resourcesMap.value.get(node.id)?.push(new Resource(await response.json()));
    } catch (error: any) {
      ToastService.addError('Error creating resource', error.message);
      console.error(error);
      throw error;
    }
  }

  function clearResources() {
    occurencesMap.value.clear();
    resourcesMap.value.clear();
  }

  const occurences = computed(() => {
    return (resourceId: string) => occurencesMap.value.get(resourceId);
  });

  const resources = computed(() => {
    return (nodeId: string) => resourcesMap.value.get(nodeId);
  });

  return {
    occurences,
    resources,

    fetchResourcesForNode,
    fetchResourceOccurences,
    clearResources,
    deleteResource,
    updateResource,
    createResource,
  };
});
