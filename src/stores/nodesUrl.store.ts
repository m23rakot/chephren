import { NodeUrl } from '@/models/nodeUrl.model';
import { defineStore } from 'pinia';

export const useNodesUrlStore = defineStore('nodesUrl', () => {
  let urls: readonly NodeUrl[] | null = null;

  function saveInLocalStorage(urls: readonly NodeUrl[]): void {
    localStorage.setItem('nodesUrl', JSON.stringify(urls));
  }

  function loadFromLocalStorage(): readonly NodeUrl[] {
    const json = localStorage.getItem('nodesUrl');
    if (!json) {
      return [];
    }
    const list = JSON.parse(json);
    return NodeUrl.fromJsonList(list);
  }

  async function importNodeUrls(file: File): Promise<readonly NodeUrl[]> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = async (e) => {
        const text = e.target?.result;
        if (!text) {
          reject(new Error('Error while reading file, text is null'));
          return;
        }
        if (typeof text !== 'string') {
          reject(new Error('Error while reading file, text is not a string'));
          return;
        }
        if (file.type === 'application/json') {
          const list = JSON.parse(text);
          urls = NodeUrl.fromJsonList(list);
          saveInLocalStorage(urls);
          resolve(urls);
        } else {
          reject(new Error('Error while reading file, file type not supported : ' + file.type));
        }
      };
      reader.readAsText(file);
    });
  }

  function exportNodeUrls(): Blob {
    const urls = getNodeUrls();
    const json = JSON.stringify(urls);
    const blob = new Blob([json], { type: 'application/json' });
    return blob;
  }

  function getNodeUrls(): readonly NodeUrl[] {
    if (!urls) {
      urls = loadFromLocalStorage();
    }
    return urls;
  }

  function addNodeUrl(url: NodeUrl): readonly NodeUrl[] {
    const u = getNodeUrls();

    urls = [...u, url];
    saveInLocalStorage(urls);
    return urls;
  }

  function deleteNodeUrl(url: NodeUrl): readonly NodeUrl[] {
    const u = getNodeUrls();

    urls = u.filter((node) => !node.equals(url));
    return urls;
  }

  return { importNodeUrls, exportNodeUrls, getNodeUrls, addNodeUrl, deleteNodeUrl };
});
