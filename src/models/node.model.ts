import { State } from './enums/state.enum';

export class Node {
  public name!: string;
  public state!: State;
  public address!: URL;

  public resourcesCount?: number;

  public loading?: boolean;

  constructor(json: Partial<Node>, loading = false) {
    Object.assign(this, json);
    this.loading = loading;
  }

  public update(json: Partial<Node>, loading = false): void {
    Object.assign(this, json);
    this.loading = loading;
  }

  get id(): string {
    const str = `${this.address.host.toLowerCase().normalize().trim()}${this.name.toLowerCase().normalize().trim()}`;

    return str
      .split('')
      .map((char) => char.charCodeAt(0).toString(16))
      .join('');
  }
}
