import type { RouteLocationRaw } from 'vue-router';

export type BreadcrumbItem = {
  route: RouteLocationRaw;
  label: string;
};
