import { Node } from './node.model';

export class NodeUrl {
  constructor(
    public url: URL,
    public name: string,
  ) {}

  static fromJson(json: any): NodeUrl {
    const url = URL.canParse(json.url)
      ? new URL(json.url)
      : new URL(`${import.meta.env.VITE_DEFAULT_PROTOCOL}://${json.url}`);
    return new NodeUrl(url, json.name);
  }

  static fromJsonList(jsonList: any[]): NodeUrl[] {
    return jsonList.map((json) => NodeUrl.fromJson(json));
  }

  public equals(other: NodeUrl): boolean {
    return this.hash === other.hash;
  }

  public contain(input: string) {
    const filters = input.split(' ');
    return filters.some(
      (filter) =>
        this.url.host
          .toLowerCase()
          .normalize('NFD')
          .includes(filter.trim().toLowerCase().normalize('NFD')) ||
        this.name
          .toLowerCase()
          .normalize('NFD')
          .includes(filter.trim().toLowerCase().normalize('NFD')),
    );
  }

  public toString(): string {
    return `${this.url} - ${this.name}`;
  }

  public isNode(node: Node): boolean {
    return this.url.host === node.address.host && this.name === node.name;
  }

  get hash(): string {
    const urlHash = this.url.host
      .toLowerCase()
      .trim()
      .normalize('NFD')
      .split('')
      .map((char) => char.charCodeAt(0).toString(16))
      .join('');
    const nameHash = this.name
      .toLowerCase()
      .trim()
      .normalize('NFD')
      .split('')
      .map((char) => char.charCodeAt(0).toString(16))
      .join('');

    return `${urlHash}${nameHash}`;
  }
}
