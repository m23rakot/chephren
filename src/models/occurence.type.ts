import type { Node } from './node.model';
import type { Resource } from './resource.model';

export type Occurence = {
  node: Node;
  resource: Resource;
  checked: boolean;
};
