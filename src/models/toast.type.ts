export type Toast = {
  id: number;
  label: string;
  sublabel: string;
  type: 'success' | 'error' | 'warning' | 'dev';
  timeout?: number;
};
