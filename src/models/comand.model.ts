export class Command {
  public id!: string;
  public command?: string;
  public date?: Date;

  constructor(json: Partial<Command>) {
    Object.assign(this, json);
  }

  equals(command: Command): boolean {
    return this.id === command.id;
  }
}
export class NewCommand {
  public command: string;
  public addresses: URL[];

  constructor() {
    this.command = '';
    this.addresses = [];
  }
}
