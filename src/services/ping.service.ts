import type { Node } from '@/models/node.model';

export class PingService {
  private static url = '/api/ping';

  static async ping(
    sourceNode: Node,
    targetNodes: Node[],
  ): Promise<{ reachable: boolean; results: { node: Node; reachable: boolean }[] }> {
    const url = `${sourceNode.address}${this.url}`;
    try {
      const response = await fetch(url, {
        method: 'GET',
        headers: {
          addresses: targetNodes.map((node) => node.address.toString()).join(','),
        },
      });

      const json = await response.json();
      if (response.ok) {
        const results: { node: Node; reachable: boolean }[] = [];
        for (const node of targetNodes) {
          results.push({ node, reachable: json.results[node.address.toString()] });
        }
        return { reachable: json.reachable, results };
      }

      throw new Error(json.message);
    } catch (e) {
      console.error(`Error pinging node ${sourceNode.name} at ${url}: ${e}`);
      return { reachable: false, results: [] };
    }
  }
}
