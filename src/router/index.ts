import { createRouter, createWebHistory } from 'vue-router';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/HomeView.vue'),
    },
    {
      path: '/info',
      name: 'info',
      component: () => import('@/views/HomeView.vue'),
    },
    {
      path: '/nodeurl',
      name: 'nodeurl',
      component: () => import('@/views/NodeUrlsView.vue'),
    },
    {
      path: '/nodes/:id',
      name: 'node',
      component: () => import('@/views/NodeView.vue'),
    },
    {
      path: '/nodes/:nodeId/:resourceId',
      name: 'resource',
      component: () => import('@/views/ResourceView.vue'),
    },
  ],
});

export default router;
