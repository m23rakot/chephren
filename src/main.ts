import './assets/main.css';

import dayjs from 'dayjs';

import { createPinia } from 'pinia';
import { createApp } from 'vue';

import App from './App.vue';
import router from './router';

const app = createApp(App);

app.config.globalProperties.$filters = {
  longDate(value: Date | dayjs.Dayjs): string {
    return dayjs(value).format('DD/MM/YYYY HH[h]mm:ss');
  },
};

app.use(createPinia());
app.use(router);

app.mount('#app');
